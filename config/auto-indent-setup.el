(require-package 'auto-indent-mode)

(after 'auto-indent-mode-autoloads
  (auto-indent-global-mode))
