(require-package 'auto-indent-mode)

(after 'ruby-mode
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (defun ruby-editing-setup () ;;
  ;;   (paredit-mode +1)          ;;
  ;;   (show-paren-mode)          ;;
  ;;   (subword-mode))            ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (defun ruby-mode-setup ()
    (auto-indent-mode)
    (whitespace-mode)
    (global-linum-mode))

  (add-hook 'ruby-mode-hook 'ruby-mode-setup))
