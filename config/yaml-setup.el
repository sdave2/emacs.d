(require-package 'yaml-mode)

(after 'yaml-mode
       (defun yaml-mode-setup ()
         (auto-indent-mode)
         (global-linum-mode)
         (auto-indent-mode))
       (add-hook 'yaml-mode-hook 'yaml-mode-setup))
